using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    TakeshiInputs inputScheme;

    PlayerInput m_playerInput;

    // Start is called before the first frame update
    void Awake()
    {
        inputScheme = new TakeshiInputs();
        m_playerInput = GetComponent<PlayerInput>();

        inputScheme.Player.Validate.performed += context => { OnValidateDoor(); };
        inputScheme.Player.Cancel.performed += context => { OnCancelDoor(); };
    }

    private void OnEnable()
    {
        inputScheme.Enable();
    }

    // Update is called once per frame
    private void OnValidateDoor()
    {
        GameManager.instance.OnManuallyValidateDoor();
    }

    void OnCancelDoor()
    {
        GameManager.instance.OnManuallyCancelDoor();
    }

    public void SwitchToUI()
    {
        inputScheme.Enable();
        m_playerInput.SwitchCurrentActionMap("UI");
    }

    public void SwitchToPlayer()
    {
        inputScheme.Enable();
        m_playerInput.SwitchCurrentActionMap("Player");
    }

    private void Update()
    {
    }
}
