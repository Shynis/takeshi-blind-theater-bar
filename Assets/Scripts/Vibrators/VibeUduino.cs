using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Uduino;

public class VibeUduino : MonoBehaviour
{
    UduinoManager u;
    UduinoDevice leftVibe = null;
    UduinoDevice rightVibe = null;

    public delegate void EventAction();
    public event EventAction OnActivateLeft;
    public event EventAction OnActivateRight;
    public event EventAction OnDeactivateLeft;
    public event EventAction OnDeactivateRight;
    public event EventAction OnSwitchVibe;

    void Start()
    {
        UduinoManager.Instance.OnBoardConnected += OnBoardConnected;
    }

    public void ActivateLeft()
    {
        Debug.Log("Vibe manager : activate left");
        UduinoManager.Instance.sendCommand(leftVibe, "bzzStart");
        OnActivateLeft?.Invoke();
    }

    public void DeactivateLeft()
    {
        UduinoManager.Instance.sendCommand(leftVibe, "bzzStop");
        OnDeactivateLeft?.Invoke();
    }

    public void ActivateRight()
    {
        Debug.Log("Vibe manager : activate right");
        UduinoManager.Instance.sendCommand(rightVibe, "bzzStart");
        OnActivateRight?.Invoke();
    }

    public void DeactivateRight()
    {
        UduinoManager.Instance.sendCommand(rightVibe, "bzzStop");
        OnDeactivateRight?.Invoke();
    }

    public void SwitchVibes()
    {
        (leftVibe, rightVibe) = (rightVibe, leftVibe);
        OnSwitchVibe?.Invoke();
    }

    public void StopVibes()
    {
        UduinoManager.Instance.sendCommand(leftVibe, "bzzStop");
        UduinoManager.Instance.sendCommand(rightVibe, "bzzStop");
    }

    // Different setups for each arduino board
    void OnBoardConnected(UduinoDevice connectedDevice)
    {
        //You can launch specific functions here
        if (connectedDevice.name == "leftVibe")
        {
            leftVibe = connectedDevice;
        }
        else if (connectedDevice.name == "rightVibe")
        {
            rightVibe = connectedDevice;
        }

        SwitchVibes();
    }
}
