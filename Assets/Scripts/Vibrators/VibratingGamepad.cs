using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VibratingGamepad
{
    public Gamepad gamepad;

    public bool isEnabled = false;

    public Vector2 freq;

    public VibratingGamepad(Gamepad gp)
    {
        gamepad = gp;

        isEnabled = false;
        freq = new Vector2(0.75f, 0.3f);
    }

    public void Activate()
    {
        gamepad.SetMotorSpeeds(freq.x, freq.y);
        isEnabled = true;
    }

    public void Deactivate()
    {
        gamepad.SetMotorSpeeds(0, 0);
        isEnabled = false;
    }

    public void SwitchOn()
    {
        if(isEnabled)
        {
            Deactivate();
        }
        else
        {
            Activate();
        }
    }

    public void SetFrequency(float lowFreq, float highFreq)
    {
        freq = new Vector2(lowFreq, highFreq);

        if(isEnabled)
        {
            Activate();
        }
    }
}
