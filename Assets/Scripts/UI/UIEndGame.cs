using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIEndGame : MonoBehaviour, ITakeshiUI
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Button okButton;

    [SerializeField] private Animation smallPanelAnimation;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        if(gameManager == null)
        {
            Debug.Log("Game Manager not found");
        }

        Activate(false);
    }

    public event Action OnExitEndScreen;
    private void ExitEndGameMenu()
    {
        ExecuteEvents.Execute(okButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

        //Exit end game menu
        OnExitEndScreen?.Invoke();
    }

    private void Activate(bool val)
    {
        canvasGroup.alpha = val ? 1 : 0;
        canvasGroup.interactable = val;
    }

    private IEnumerator WaitForDropIn()
    {
        canvasGroup.alpha = 1;

        smallPanelAnimation.Play("dropSmallPanel");
        yield return new WaitForSeconds(0.5f);

        canvasGroup.interactable = true;
    }

    private IEnumerator WaitForLift()
    {
        canvasGroup.interactable = false;

        smallPanelAnimation.Play("liftSmallPanel");
        yield return new WaitForSeconds(0.3f);

        canvasGroup.alpha = 0;
    }

    // ITakeshiUI interface methods

    public void LeftDown() {}
    public void LeftUp() {}

    public void RightDown() {}
    public void RightUp() {}

    public void SpaceDown()
    {
        ExitEndGameMenu();
    }
    public void SpaceUp() {}

    public void SwitchFrom()
    {
        // Activate(false);

        StartCoroutine(WaitForLift());
    }
    public void SwitchTo()
    {
        scoreText.text = GameManager.instance.Score.ToString();

        StartCoroutine(WaitForDropIn());
    }

    public void Initialize()
    {
        canvasGroup.enabled = true;
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
    }
}
