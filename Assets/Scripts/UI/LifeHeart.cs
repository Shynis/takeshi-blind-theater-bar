using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeHeart : MonoBehaviour
{
    [SerializeField] private Image redHeart;
    [SerializeField] private Image greyHeart;

    // Start is called before the first frame update
    private void Start()
    {
        redHeart.enabled = true;
    }

    public void ActivateHeart()
    {
        redHeart.enabled = true;
    }

    public void DeactivateHeart()
    {
        redHeart.enabled = false;
    }
}
