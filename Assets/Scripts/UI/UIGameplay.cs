using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGameplay : MonoBehaviour, ITakeshiUI
{
    [SerializeField] private CanvasGroup canvasGroup;

    [SerializeField] private List<LifeHeart> heartList;
    private int currentHeartIndex = 0;

    [SerializeField] private TextMeshProUGUI scoreText;
    private string baseScoreText = "Score: ";

    private GameManager gameManager;
    private VibeUduino vibeManager;

    private void OnEnable()
    {
        gameManager = FindObjectOfType<GameManager>();
        vibeManager = FindObjectOfType<VibeUduino>();

        SubscribeToEvent();
    }

    private void SubscribeToEvent()
    {
        gameManager.OnGameStarted += StartGame;

        gameManager.OnDoorPassed += PassDoor;
        gameManager.OnHeartLost += LoseHeart;
        gameManager.OnGameEnded += EndGame;
    }

    private void UpdateScore()
    {
        scoreText.text = baseScoreText + gameManager.Score;
    }

    private void StartGame()
    {
        canvasGroup.alpha = 1;

        // Reset score
        UpdateScore();

        // Display the correct number of lives
        int i = 0;

        while(i < gameManager.HeartLeft)
        {
            heartList[i].ActivateHeart();
            i++;
        }

        currentHeartIndex = gameManager.HeartLeft - 1;
        // Debug.Log("currentheartindex: " + currentHeartIndex);
    }

    private void PassDoor()
    {
        // Add Animation
        UpdateScore();
    }

    private void LoseHeart()
    {
        //Add Animation

        heartList[currentHeartIndex].DeactivateHeart();
        currentHeartIndex--;
    }

    public event Action OnGameEnded;
    private void EndGame()
    {
        canvasGroup.alpha = 0;

        OnGameEnded?.Invoke();
    }

    private void OnDestroy()
    {
        UnsubscribeToEvent();
    }

    private void UnsubscribeToEvent()
    {
        gameManager.OnGameStarted -= StartGame;

        gameManager.OnDoorPassed -= PassDoor;
        gameManager.OnHeartLost -= LoseHeart;
        gameManager.OnGameEnded -= EndGame;
    }

    public void LeftDown()
    {
        vibeManager.ActivateLeft();
    }
    public void LeftUp()
    {
        vibeManager.DeactivateLeft();
    }

    public void RightDown()
    {
        vibeManager.ActivateRight();
    }
    public void RightUp()
    {
        vibeManager.DeactivateRight();
    }

    public void SpaceDown() {}
    public void SpaceUp() {}

    public void SwitchFrom()
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;

        vibeManager.StopVibes();
    }

    public void SwitchTo()
    {
        canvasGroup.alpha = 1;

        gameManager.StartGame();
    }

    public void Initialize()
    {
        canvasGroup.enabled = true;
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
    }

}
