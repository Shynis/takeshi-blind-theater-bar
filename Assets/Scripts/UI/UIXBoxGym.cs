using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIXBoxGym : MonoBehaviour
{
    [SerializeField] private Slider lowFreqSlider;
    [SerializeField] private TextMeshProUGUI lowFreqText;
    private string baseLowFreqText = "Low frequency : ";

    [SerializeField] private Slider highFreqSlider;
    [SerializeField] private TextMeshProUGUI highFreqText;
    private string baseHighFreqText = "High frequency : ";

    private VibratingGamepadManager gamepadManager;

    private void Start()
    {
        gamepadManager = FindObjectOfType<VibratingGamepadManager>();

        lowFreqSlider.onValueChanged.AddListener(delegate { OnLowFreqChanged(); });
        highFreqSlider.onValueChanged.AddListener(delegate { OnHighFreqChanged(); });

        lowFreqText.text = baseLowFreqText + lowFreqSlider.value;
        highFreqText.text = baseLowFreqText + highFreqSlider.value;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            gamepadManager.ActivateLeft();
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            gamepadManager.DeactivateLeft();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            gamepadManager.ActivateRight();
        }

        if (Input.GetKeyUp(KeyCode.Return))
        {
            gamepadManager.DeactivateRight();
        }
    }

    public void OnLowFreqChanged()
    {
        gamepadManager.lowFrequency = lowFreqSlider.value;
        lowFreqText.text = baseLowFreqText + lowFreqSlider.value;

        gamepadManager.UpdateVibrationFreq();
    }

    public void OnHighFreqChanged()
    {
        gamepadManager.highFrequency = highFreqSlider.value;
        highFreqText.text = baseHighFreqText + highFreqSlider.value;

        gamepadManager.UpdateVibrationFreq();
    }
}
