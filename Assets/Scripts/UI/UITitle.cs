using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITitle : MonoBehaviour, ITakeshiUI
{
    [SerializeField] private CanvasGroup canvasGroup;

    [SerializeField] private Button challengeButton;
    [SerializeField] private Button zenButton;

    [SerializeField] private Animation bigPanelAnimation;

    private Button currentButton;

    public event Action OnChallengeModeSelected;
    public void SelectChallengeMode()
    {
        OnChallengeModeSelected?.Invoke();
    }

    public event Action OnZenModeSelected;
    public void SelectZenMode()
    {
        OnZenModeSelected?.Invoke();
    }

    private void SelectChallengeButton()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(challengeButton.gameObject);
        challengeButton.Select();
        challengeButton.OnSelect(null);
        currentButton = challengeButton;
    }

    private void SelectZenButton()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(zenButton.gameObject);
        zenButton.Select();
        zenButton.OnSelect(null);
        currentButton = zenButton;
    }

    // ITakeshiUI interface methods

    public void LeftDown()
    {
        // SelectChallengeButton();
    }
    public void LeftUp() {}

    public void RightDown()
    {
        // SelectZenButton();
    }
    public void RightUp() {}

    public void SpaceDown()
    {
        // Validate chosen mode
        // ExecuteEvents.Execute(currentButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

    }
    public void SpaceUp() {}

    public void SwitchFrom()
    {
        canvasGroup.interactable = false;
        bigPanelAnimation.Play("LiftOutBigPanel");

        StartCoroutine(WaitForLiftOut());
    }

    public void SwitchTo()
    {
        canvasGroup.enabled = true;
        canvasGroup.alpha = 1;

        bigPanelAnimation.Play("DropInBigPanel");

        StartCoroutine(WaitForDropIn());
    }

    private IEnumerator WaitForLiftOut()
    {
        yield return new WaitForSeconds(0.7f);

        canvasGroup.alpha = 0;
    }

    private IEnumerator WaitForDropIn()
    {
        yield return new WaitForSeconds(1f);

        canvasGroup.interactable = true;
        SelectChallengeButton();
    }

    public void Initialize()
    {
        canvasGroup.enabled = true;
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
    }
}
