using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISettings : MonoBehaviour, ITakeshiUI
{
    [SerializeField] private CanvasGroup canvasGroup;

    [SerializeField] private CanvasGroup speedCanvasGroup;
    [SerializeField] private Slider speedSlider;
    private Vector3 initialSpeedTransform;

    [SerializeField] private CanvasGroup precisionCanvasGroup;
    [SerializeField] private Slider precisionSlider;
    private Vector3 initialPrecisionTransform;

    [SerializeField] private CanvasGroup contrastCanvasGroup;
    [SerializeField] private Slider contrastSlider;
    private Vector3 initialContrastTransform;

    [SerializeField] private Button okButton;

    [Header("Animation")]
    [SerializeField] private Animation pannelAnimation;
    [SerializeField] private Animation speedAnimation;
    [SerializeField] private Animation precisionAnimation;
    [SerializeField] private Animation contrastAnimation;

    private bool isActive;

    private enum SettingPanel { Null, Speed, Precision, Contrast, COUNT };
    private SettingPanel currentSetting;
    private Slider currentSlider;

    private GameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();

        Activate(false);

        Debug.Log("UI Settings Awake");

        initialSpeedTransform = speedCanvasGroup.transform.position;
        initialPrecisionTransform = precisionCanvasGroup.transform.position;
        initialContrastTransform = contrastCanvasGroup.transform.position;
    }

    private void Activate(bool val)
    {
        Debug.Log("Activate: " + val);

        isActive = val;
        canvasGroup.alpha = val ? 1 : 0;
        canvasGroup.interactable = val;
    }

    // Speed setting

    private void ShowSpeed(bool val)
    {
        // currentSetting = SettingPanel.Speed;

        speedCanvasGroup.enabled = true;
        speedCanvasGroup.alpha = val ? 1 : 0;
        speedCanvasGroup.interactable = val;
    }

    private void SelectSpeedSlider()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(speedSlider.gameObject);

        speedSlider.Select();
        speedSlider.OnSelect(null);
    }

    private void ValidateSpeedSettings()
    {
        int speed = (int)speedSlider.value;
        gameManager.SetGameSpeed(speed);
    }

    private IEnumerator TransitionFromSpeedToPrecision()
    {
        Debug.Log("Transition speed >> precision");
        speedAnimation.Play("slideOutSettings");

        precisionCanvasGroup.alpha = 1;
        precisionAnimation.Play("slideInSettings");

        yield return new WaitForSeconds(0.3f);

        speedCanvasGroup.alpha = 0;
        speedCanvasGroup.interactable = false;
        speedCanvasGroup.transform.position = initialSpeedTransform;

        precisionCanvasGroup.interactable = true;
    }

    // Precision setting

    private void ShowPrecision(bool val)
    {
        precisionCanvasGroup.enabled = true;
        precisionCanvasGroup.alpha = val ? 1 : 0;
        precisionCanvasGroup.interactable = val;
    }

    private void SelectPrecisionSlider()
    {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(precisionSlider.gameObject);

            precisionSlider.Select();
            precisionSlider.OnSelect(null);
    }

    private void ValidatePrecisionSettings()
    {
        float precision = 1 - (precisionSlider.value / (precisionSlider.maxValue - precisionSlider.minValue));
        gameManager.SetPrecision(precision);
    }

    private IEnumerator TransitionFromPrecisionToContrast()
    {
        precisionAnimation.Play("slideOutSettings");

        contrastCanvasGroup.alpha = 1;
        contrastAnimation.Play("slideInSettings");

        yield return new WaitForSeconds(0.3f);

        precisionCanvasGroup.alpha = 0;
        precisionCanvasGroup.interactable = false;

        precisionCanvasGroup.transform.position = initialPrecisionTransform;

        contrastCanvasGroup.interactable = true;
    }

    private void ShowContrast(bool val)
    {
        // currentSetting = SettingPanel.Contrast;

        contrastCanvasGroup.enabled = true;
        contrastCanvasGroup.alpha = val ? 1 : 0;
        contrastCanvasGroup.interactable = val;
    }

    private void SelectContrastSlider()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(contrastSlider.gameObject);

        contrastSlider.Select();
        contrastSlider.OnSelect(null);
    }

    private void ValidateContrastSettings()
    {
        float contrast = contrastSlider.value;
        gameManager.SetContrast(contrast);
    }

    // Animation

    private IEnumerator WaitForDropIn()
    {
        Debug.Log("WaitForDropIn");

        yield return null;

        canvasGroup.alpha = 1;
        canvasGroup.interactable = false;

        speedCanvasGroup.alpha = 1;

        pannelAnimation.Play("DropInSimple");

        yield return new WaitForSeconds(0.5f);

        Debug.Log("Set SettingPanel to speed");
        currentSetting = SettingPanel.Speed;

        SelectSpeedSlider();

        ShowSpeed(true);
        ShowPrecision(false);
        ShowContrast(false);

        canvasGroup.interactable = true;
    }

    private IEnumerator WaitForLiftOut()
    {
        Debug.Log("WaitForLiftOut");

        canvasGroup.interactable = false;
        pannelAnimation.Play("LiftOutBigPanel");

        yield return new WaitForSeconds(0.5f);

        contrastCanvasGroup.alpha = 0;
        contrastCanvasGroup.interactable = true;
        canvasGroup.alpha = 0;
    }

    // ITakeshiUI interface methods

    public void LeftDown() {}
    public void LeftUp() {}

    public void RightDown() {}
    public void RightUp() {}

    public event Action OnSettingsSet;

    public void SpaceDown()
    {
        // ExecuteEvents.Execute(okButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

        Debug.Log("Start switch case");

        switch(currentSetting)
        {
            case SettingPanel.Null:
            {
                Debug.Log("SettingPanel to Null");

                break;
            }

            case SettingPanel.Speed:
            {
                ValidateSpeedSettings();

                currentSetting = SettingPanel.Precision;

                contrastCanvasGroup.alpha = 0;
                contrastCanvasGroup.interactable = false;

                StartCoroutine(TransitionFromSpeedToPrecision());

                SelectPrecisionSlider();

                Debug.Log("Validated speed, >> Precision");

                break;
            }

            case SettingPanel.Precision:
            {
                ValidatePrecisionSettings();

                currentSetting = SettingPanel.Contrast;
                ShowSpeed(false);
                // ShowPrecision(false);
                // ShowContrast(true);

                StartCoroutine(TransitionFromPrecisionToContrast());

                SelectContrastSlider();

                Debug.Log("Validated precision, >> contrast");

                break;
            }

            case SettingPanel.Contrast:
            {
                ValidateContrastSettings();

                currentSetting = SettingPanel.Null;
                ShowSpeed(false);
                ShowPrecision(false);
                // ShowContrast(false);
                contrastCanvasGroup.interactable = false;

                Debug.Log("Validated contrast, >> start game");

                OnSettingsSet?.Invoke();

                break;
            }

            case SettingPanel.COUNT:
            {
                Debug.LogWarning("Bad SettingPanel value");
                break;
            }
        }
    }
    public void SpaceUp() {}

    public void SwitchFrom()
    {
        // Activate(false);

        StartCoroutine(WaitForLiftOut());
    }

    public void SwitchTo()
    {
        StartCoroutine(WaitForDropIn());
    }

    private IEnumerator SwitchToCo()
    {
        yield return new WaitForSeconds(0.1f);

        Activate(true);

        currentSetting = SettingPanel.Speed;

        ShowSpeed(true);
        ShowPrecision(false);
        ShowContrast(false);
    }

    public void Initialize()
    {
        canvasGroup.enabled = true;
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
    }
}
