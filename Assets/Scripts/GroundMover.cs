using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMover : MonoBehaviour
{
    [SerializeField] Material groundMaterial;
    [SerializeField] float speed;

    [SerializeField] float lastMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnGameStarted += Initialize;
    }

    void Initialize()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Panel panel = GameManager.instance.m_panelSpawner.m_panel;
        if (panel)
        {
            lastMultiplier = panel.GetComponent<PanelMover>().m_speed;
        }
        if (CustomTimer.instance.bIsActive) {
            groundMaterial.SetFloat("_Offset", groundMaterial.GetFloat("_Offset") + Time.deltaTime * lastMultiplier * speed);
        }
    }
}
