using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSilhouette : MonoBehaviour
{
    [SerializeField] private bool displaySilhouette = true;
    private MeshRenderer silhouetteSprite;

    [SerializeField] private float moveSpeed = 1f;

    private Vector3 silhouettePosition;

    private PositionManager positionManager;

    private void Awake()
    {
        positionManager = FindObjectOfType<PositionManager>();
        silhouetteSprite = GetComponentInChildren<MeshRenderer>();
    }

    private void Start()
    {
        silhouettePosition = new Vector3(0, 0, 0);
    }

    private void Update()
    {
        if(displaySilhouette)
        {

            silhouetteSprite.enabled = true;
            MoveSilhouette();
        }
        else
        {
            silhouetteSprite.enabled = false;
        }
    }

    private void MoveSilhouette()
    {
        Vector2 headPosition = positionManager.headPosition;

        silhouettePosition.x = headPosition.x;
        //silhouettePosition.y = headPosition.y;
        silhouettePosition.y = transform.position.y;
        silhouettePosition.z = Camera.main.transform.position.z + positionManager.silhouetteDepth ;

        float step = moveSpeed * Time.deltaTime;

        this.transform.position = Vector3.MoveTowards(transform.position, silhouettePosition, step);
    }
}
