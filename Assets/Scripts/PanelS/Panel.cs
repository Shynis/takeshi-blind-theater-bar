using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PanelRenderer))]
[RequireComponent(typeof(PanelMover))]
public class Panel : MonoBehaviour
{
    public float doorPosition; // A value in 0..1 that describes the door location
    public float spawnDate;
    public float realSpawnDate;
    public float duration;

    [System.NonSerialized] public PanelRenderer panelRenderer;
    [System.NonSerialized] public PanelMover panelMover;

    public delegate void PanelInitializedAction();
    public event PanelInitializedAction OnPanelInitialized;


    private void Awake()
    {
        panelMover = GetComponent<PanelMover>();
        panelRenderer = GetComponent<PanelRenderer>();
    }

    private void Update()
    {
        if (CustomTimer.instance.time - spawnDate > duration)
        {
            Destroy(this.gameObject);
        }
    }

    public void Initialize(float v_duration, float v_doorPosition)
    {
        duration = v_duration;
        doorPosition = v_doorPosition;
        spawnDate = CustomTimer.instance.time;
        realSpawnDate = Time.time;

        OnPanelInitialized?.Invoke();
    }

}
