using UnityEngine;

public class GameplayBuildControls : MonoBehaviour
{
    private VibratingGamepadManager gamepadManager;
    private GameManager gameManager;

    private void Start()
    {
        gamepadManager = FindObjectOfType<VibratingGamepadManager>();
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Debug.Log("Left arrow down");
            gamepadManager.ActivateLeft();
        }

        if(Input.GetKeyUp(KeyCode.LeftArrow))
        {
            gamepadManager.DeactivateLeft();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            Debug.Log("Right arrow down");
            gamepadManager.ActivateRight();
        }

        if(Input.GetKeyUp(KeyCode.RightArrow))
        {
            gamepadManager.DeactivateRight();
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("S down");
            gamepadManager.SwitchControllers();
        }

        if(Input.GetKeyDown("space"))
        {
            Debug.Log("Space down");
            gameManager.StartGame();
        }
    }
}
