using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpawnAnimator : MonoBehaviour
{
    Vector3 initialScale;
    [SerializeField] AnimationCurve curve;
    [SerializeField] float duration;
    [SerializeField] bool randomizedX;
    [SerializeField] float timeOffsetRange;
    [SerializeField] float timeDelay;
    float timeOffset;
    [SerializeField] bool autoActivate;

    float spawnTime;
    bool initialized;


    private void Awake()
    {

        initialScale = transform.localScale;
        if (autoActivate)
        {
            Initialize();
        }
        
    }


    // Update is called once per frame
    void Update()
    {
        if (initialized)
        {
            transform.localScale = new Vector3(initialScale.x, initialScale.y * curve.Evaluate((Time.time - timeDelay - timeOffset - spawnTime) / duration), initialScale.z);
        }
    }

    public void Initialize()
    {
        initialized = true;
        if (randomizedX)
        {
            initialScale = Vector3.Scale(initialScale,
                    new Vector3(Random.Range(0, 2) * 2 - 1, 1, 1));
        }
        spawnTime = Time.time;
        timeOffset = Random.Range(0, timeOffsetRange);
        transform.localScale = new Vector3(initialScale.x, 0, initialScale.z);
    }
}
