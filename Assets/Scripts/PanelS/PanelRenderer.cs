using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Panel))]
public class PanelRenderer : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_panelList;
    [SerializeField] private GameObject m_doorGameObject;
    [SerializeField] public Vector3 m_spawnLocation;

    [Header("Door")]
    [SerializeField] private float m_doorSpawnY;
    [SerializeField] private float m_doorSpawnXRange;

    [Header("Props")]
    [SerializeField] private List<GameObject> m_props;
    [SerializeField] private Vector3 m_propsSpawnRangeLow;
    [SerializeField] private Vector3 m_propsSpawnRangeHigh;
    [SerializeField] private float m_propsSpawnDoorWidth;
    [SerializeField] private Vector2Int m_propSpawnNumberRange;
    private List<float> m_spawnedPropsX;
    [SerializeField] private float m_spawnedPropsWidth;

    [SerializeField] private List<GameObject> m_propsPool;

    private GameObject m_door;
    private GameObject m_panelVisual;

    [Header("Curtains")]
    [SerializeField] private float curtainsSpawnLocation;
    [SerializeField] private float curtainsEndLocation;

    private Panel m_panel;

    [Header("Rail")]
    private MaterialPropertyBlock _propBlock;
    [SerializeField] private float m_railDuration;
    [SerializeField] private float m_railOffset;
    [SerializeField] Material m_railMaterial;

    private List<GameObject> instantiatedObjects = new List<GameObject>();

    private void OnEnable()
    {
        m_panel.OnPanelInitialized += Initialize;
    }

    private void OnDisable()
    {
        m_panel.OnPanelInitialized -= Initialize;
    }

    private void OnDestroy()
    {
        if (!this.gameObject.scene.isLoaded) return;
        foreach (GameObject instantiated in instantiatedObjects)
        {
            instantiated.transform.parent = null;
            instantiated.transform.position = new Vector3(0, -100, 0);
        }
    }

    private void Awake()
    {
        m_panel = GetComponent<Panel>();

        transform.position = m_spawnLocation;
        transform.rotation = Quaternion.identity;

        int panelId = Random.Range(0, m_panelList.Count);
        m_panelVisual = Instantiate(m_panelList[panelId], transform, true);
        m_panelVisual.transform.localPosition = Vector3.zero;
        m_door = Instantiate(m_doorGameObject, m_panelVisual.transform, true);


        //int propsPoolId = Random.Range(0, m_propsPool.Count);
        //Instantiate(m_propsPool[propsPoolId], m_panelVisual.transform);
        PooledPrefab propsPool = PoolManager.instance.GetPropPool();
        propsPool.prefab.transform.parent = m_panelVisual.transform;
        propsPool.prefab.transform.localPosition = propsPool.initialPositon;
        propsPool.prefab.transform.localScale = new Vector3(1, 1, 1);
        foreach (SpawnAnimator animator in propsPool.prefab.GetComponentsInChildren<SpawnAnimator>())
        {
            animator.Initialize();
        }
        instantiatedObjects.Add(propsPool.prefab);

        //_propBlock = new MaterialPropertyBlock();
        //_propBlock.SetFloat("_CutoffOffset", -1);
        //m_door.GetComponent<Door>().railRenderer.SetPropertyBlock(_propBlock);
        m_railMaterial.SetFloat("_CutoffOffset", -30);

        Vector3 curtainPosition = Curtains.instance.transform.position;
        Curtains.instance.transform.position = new Vector3(curtainPosition.x, curtainPosition.y,
            curtainsSpawnLocation);
        Curtains.instance.GetComponent<Animation>().Play();
    }

    private void Update()
    {
        Vector3 curtainPosition = Curtains.instance.transform.position;
        Curtains.instance.transform.position = new Vector3(curtainPosition.x, curtainPosition.y,
            Mathf.Lerp(curtainsSpawnLocation, curtainsEndLocation,
            (CustomTimer.instance.time - m_panel.spawnDate) / m_panel.duration));

        //_propBlock.SetFloat("_CutoffOffset", Mathf.Lerp(-1, 1, (Time.time - m_panel.realSpawnDate - m_railOffset) / m_railDuration));
        //m_door.GetComponent<Door>().railRenderer.SetPropertyBlock(_propBlock);
        m_railMaterial.SetFloat("_CutoffOffset", Mathf.Lerp(-30, 30, (Time.time - m_panel.realSpawnDate - m_railOffset) / m_railDuration));
    }

    void Initialize()
    {
        m_door.transform.localPosition = new Vector3((m_panel.doorPosition - 0.5f) * m_doorSpawnXRange, m_doorSpawnY, -0.3f);


        int propNumber = Random.Range(m_propSpawnNumberRange.x, m_propSpawnNumberRange.y);
        m_spawnedPropsX = new List<float>();
        //Debug.Log("door" + m_panel.doorPosition);

        for (int i = 0; i < propNumber; i++)
        {


            // Avoid door
            float randomValueX = Random.value;
            randomValueX *= (1 - m_propsSpawnDoorWidth);
            if (randomValueX > m_panel.doorPosition - m_propsSpawnDoorWidth / 2)
            {
                randomValueX += m_propsSpawnDoorWidth;
            }


            bool spawn = true;
            foreach (float spawnedX in m_spawnedPropsX)
            {
                if (Mathf.Abs(randomValueX - spawnedX) < m_spawnedPropsWidth)
                {
                    spawn = false;
                }
            }

            m_spawnedPropsX.Add(randomValueX);

            //Debug.Log(randomValueX);
            if (spawn)
            {
                //int propID = Random.Range(0, m_props.Count);

                //GameObject spawnedProp = Instantiate(m_props[propID], m_panelVisual.transform);

                //spawnedProp.transform.localPosition =
                //    new Vector3(Mathf.Lerp(m_propsSpawnRangeLow.x, m_propsSpawnRangeHigh.x, randomValueX),
                //    Random.Range(m_propsSpawnRangeLow.y, m_propsSpawnRangeHigh.y),
                //    Random.Range(m_propsSpawnRangeLow.z, m_propsSpawnRangeHigh.z));

                //spawnedProp.transform.localScale = Vector3.Scale(spawnedProp.transform.localScale,
                //    new Vector3(Random.Range(0, 2) * 2 - 1, 1, 1));

                PooledPrefab prop = PoolManager.instance.GetProp();
                prop.prefab.transform.parent = m_panelVisual.transform;
                prop.prefab.transform.localPosition = new Vector3(Mathf.Lerp(m_propsSpawnRangeLow.x, m_propsSpawnRangeHigh.x, randomValueX),
                    Random.Range(m_propsSpawnRangeLow.y, m_propsSpawnRangeHigh.y),
                    Random.Range(m_propsSpawnRangeLow.z, m_propsSpawnRangeHigh.z));
                prop.prefab.transform.localScale = Vector3.Scale(prop.prefab.transform.localScale,
                    new Vector3(Random.Range(0, 2) * 2 - 1, 1, 1));
                prop.prefab.GetComponent<SpawnAnimator>().Initialize();
                instantiatedObjects.Add(prop.prefab);
            }
        }


    }

}
