using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Panel))]
public class PanelSoundPlayer : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_animationCurve;
    AudioSource m_audioSource;
    [SerializeField] float volume;

    [SerializeField] Vector2 sfxPitchRange = new Vector2(0.8f, 1.5f);

    Panel m_panel;

    private void Awake()
    {
        m_panel = GetComponent<Panel>();
        m_audioSource = GetComponent<AudioSource>();

        m_audioSource.pitch = Random.Range(sfxPitchRange.x, sfxPitchRange.y);
    }

    private void Update()
    {
        m_audioSource.volume = volume * m_animationCurve.Evaluate((CustomTimer.instance.time - m_panel.spawnDate)/m_panel.duration);
    }
}
