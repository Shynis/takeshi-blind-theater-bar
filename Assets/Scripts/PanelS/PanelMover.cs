using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Panel))]
public class PanelMover : MonoBehaviour
{
    public float m_speed;
    [SerializeField] private float m_destroyDepth = -5;
    private Panel m_panel;
    private Vector3 m_direction;

    private void Awake()
    {
        m_panel = GetComponent<Panel>();
    }

    private void OnEnable()
    {
        m_panel.OnPanelInitialized += Initialize;
    }

    private void OnDisable()
    {
        m_panel.OnPanelInitialized -= Initialize;
    }


    void Initialize()
    {
        m_speed = ((m_panel.panelRenderer.m_spawnLocation - Camera.main.transform.position).magnitude 
            * (m_panel.panelRenderer.m_spawnLocation.z - m_destroyDepth)/(m_panel.panelRenderer.m_spawnLocation.z 
            - Camera.main.transform.position.z)) / m_panel.duration;
        m_direction = (Camera.main.transform.position - m_panel.panelRenderer.m_spawnLocation).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        if (CustomTimer.instance.bIsActive)
        {
            transform.position = transform.position +
                m_direction * Time.deltaTime * m_speed;
        }
    }

}
