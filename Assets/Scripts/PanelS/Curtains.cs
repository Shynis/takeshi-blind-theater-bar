using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curtains : MonoBehaviour
{

    static private Curtains m_instance;
    static public Curtains instance { get { return m_instance; } private set { m_instance = value; } }

    // Start is called before the first frame update
    void Start()
    {
        if (m_instance == null)
        {
            m_instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
