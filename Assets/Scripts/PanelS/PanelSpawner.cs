using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanelSpawner : MonoBehaviour
{
    [SerializeField] private float m_spawnDelay;
    [SerializeField] private float m_panelDuration;
    [System.NonSerialized] public float speedMultiplier;


    [SerializeField] private Panel m_panelPrefab;

    [SerializeField] private Vector2 m_doorPositionRange;

    [SerializeField] private float m_startupDelay;

    [SerializeField] private float m_minimalPositionDelta;

  
    public delegate void PanelSpawnAction(float duration, float doorPosition, Panel panel);
    public event PanelSpawnAction OnPanelSpawn;

    private bool m_hasStarted;

    float timer;
    public int panelSpawned;

    float lastDoorPosition;

    public Panel m_panel;

    private void Start()
    {
        
        GameManager.instance.OnGameStarted += Initialize;
    }

    // Start is called before the first frame update
    private void Initialize()
    {
        timer = -1;
        panelSpawned = 0;
        lastDoorPosition = m_doorPositionRange.x - m_minimalPositionDelta;
        speedMultiplier = GameManager.instance.difficultySettingsMultiplier[GameManager.instance.gameSettings.gameSpeed];
        StartCoroutine(StartupCoroutine());

        float time = 0;
        float tmpSpeedMult = speedMultiplier;
        int i = 0;
        while(time < 120f && i < 100)
        {

            tmpSpeedMult = tmpSpeedMult *
           GameManager.instance.speedMultiplierCurve.Evaluate(i / GameManager.instance.speedMultiplierCurveScale.x)
           * GameManager.instance.speedMultiplierCurveScale.y;
            i++;
            float duration = m_panelDuration * tmpSpeedMult;
            Debug.Log($"Door {i} duration : {duration} - {time}");
            time += duration;
        }
    }

    public void Update()
    {
        if (m_hasStarted)
        {
            if (CustomTimer.instance.bIsActive)
            {
                timer -= Time.deltaTime;
            }
            if (timer < 0)
            {
                Spawn();
                timer = m_spawnDelay * speedMultiplier;
            }
        }


        if (m_spawnDelay < m_panelDuration)
        {
            throw new System.Exception("Spawn delay is smaller than the panel duration -> undefined behaviour");
        }
    }


    void Spawn()
    {

        speedMultiplier = speedMultiplier *
            GameManager.instance.speedMultiplierCurve.Evaluate(panelSpawned / GameManager.instance.speedMultiplierCurveScale.x)
            * GameManager.instance.speedMultiplierCurveScale.y;


        panelSpawned++;
        //To be modified
        float doorPosition;
        int i = 0;
        do
        {
            doorPosition = Random.Range(m_doorPositionRange.x, m_doorPositionRange.y);
            i++;
        }
        while (Mathf.Abs(doorPosition - lastDoorPosition) < m_minimalPositionDelta && i < 10);
        
        lastDoorPosition = doorPosition;

        m_panel = Instantiate(m_panelPrefab);
        float duration = m_panelDuration * speedMultiplier;
        m_panel.Initialize(duration, doorPosition);
        //
        Debug.Log($"Door {panelSpawned} duration : {duration}");

        OnPanelSpawn?.Invoke(duration, doorPosition, m_panel);
    }

    IEnumerator StartupCoroutine()
    {
        yield return new WaitForSeconds(m_startupDelay);
        m_hasStarted = true;
    }
}
