using OpenCvSharp;
using OpenCvSharp.Demo;
using UnityEngine;
using Rect = OpenCvSharp.Rect;

public class HeadLocationManager : MonoBehaviour
{
    // public List<Vector3> HeadPosition {get; private set;}

    [SerializeField] private InteractiveZone[] zones;
    public Vector2 HeadPosition {get; private set;}

    // public int HeadNumber {get; private set;}

    // private int firstFreeIndex;

    private FaceDetectorScene faceDetectorScene;

    [SerializeField] float time_delay = 0.5f;
    float[] times = new float[3];

    private void Awake() {
        faceDetectorScene = FindObjectOfType<FaceDetectorScene>();
        times[0] = time_delay;
        times[1] = time_delay;
        times[2] = time_delay;
    }

    private void Update() {
        float zoneWidth = Screen.width / (float) zones.Length;
        Vector2 textureSize = faceDetectorScene.TextureSize;

        Vector2 factor = new Vector2(Screen.width / textureSize.x, Screen.height / textureSize.y);

        for (int i = 0; i < zones.Length; i++) {
            InteractiveZone zone = zones[i];
            bool found = false;
            for (int j = 0; j < faceDetectorScene.Faces.Count && !found; j++) {
                Rect rect = faceDetectorScene.Faces[j];
                rect.Size = new Size(rect.Size.Width * factor.x, rect.Size.Height * factor.y);
                rect.X = (int) (rect.X * factor.x);
                rect.Y = Screen.height - (int) (rect.Y * factor.y);

                if (IsPointInsideZone(i, zoneWidth, rect.Center)) {
                    times[i] = time_delay;
                    zone.ActivateZone();
                    found = true;
                }
            }

            if (!found) {
                times[i] -= Time.deltaTime;
                if (times[i] < 0)
                    zone.DeactivateZone();
            }
        }
    }

    private static bool IsPointInsideZone(int zoneIndex, float zoneWidth, Point center) {
        return center.X >= zoneWidth * zoneIndex && center.X < zoneWidth * (zoneIndex + 1);
    }

    // Used for debug
    private void GetMousePosition()
    {
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        HeadPosition = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos() {
        if (!Application.isPlaying) {
            return;
        }

        float zoneWidth = Screen.width / (float) zones.Length;
        Vector2 textureSize = faceDetectorScene.TextureSize;

        Vector2 factor = new Vector2(Screen.width / textureSize.x, Screen.height / textureSize.y);

        for (int i = 0; i < zones.Length; i++) {
            InteractiveZone zone = zones[i];
            for (int j = 0; j < faceDetectorScene.Faces.Count; j++) {
                Rect rect = faceDetectorScene.Faces[j];
                rect.Size = new Size(rect.Size.Width * factor.x, rect.Size.Height * factor.y);
                rect.X = (int) (rect.X * factor.x);
                rect.Y = Screen.height - (int) (rect.Y * factor.y);

                if (IsPointInsideZone(i, zoneWidth, rect.Center)) {
                    Gizmos.color = i switch {
                        0 => Color.red,
                        1 => Color.green,
                        _ => Color.blue
                    };
                }
                Gizmos.DrawWireSphere(new Vector3(rect.Center.X, rect.Center.Y, 0.0f), 100.0f);
            }
        }
    }
#endif
}
