using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITakeshiUI
{
    public void LeftDown();
    public void LeftUp();

    public void RightDown();
    public void RightUp();

    public void SpaceDown();
    public void SpaceUp();

    public void SwitchFrom();
    public void SwitchTo();

    public void Initialize();
}

public class UIManager : MonoBehaviour
{
    public UITitle title;
    public UISettings settings;
    public UIGameplay gameplay;
    public UIEndGame end;

    private ITakeshiUI currentUI;

    private GameManager gameManager;

    private PlayerController playerController;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        playerController = FindObjectOfType<PlayerController>();
        SubscribeToEvents();
    }

    private void Start()
    {
        title.Initialize();
        settings.Initialize();
        gameplay.Initialize();
        end.Initialize();

        currentUI = title;
        currentUI.SwitchTo();

        playerController.SwitchToUI();
    }

    private void EnterSettings()
    {
        currentUI.SwitchFrom();

        currentUI = settings;
        currentUI.SwitchTo();
    }

    private void EnterChallengeMode()
    {
        currentUI.SwitchFrom();

        playerController.SwitchToPlayer();

        gameManager.gameSettings.zenModeSettings = false;
        currentUI = gameplay;
        currentUI.SwitchTo();
    }

    private void EnterZenMode()
    {
        currentUI.SwitchFrom();

        playerController.SwitchToPlayer();

        gameManager.gameSettings.gameSpeed = 3;
        gameManager.gameSettings.zenModeSettings = true;
        currentUI = gameplay;
        currentUI.SwitchTo();
    }

    private void EnterEndScreen()
    {
        Debug.Log("UIManager - Enter End Screen");

        playerController.SwitchToUI();

        currentUI.SwitchFrom();

        currentUI = end;
        currentUI.SwitchTo();
    }

    private void ReturnToTitle()
    {
        currentUI.SwitchFrom();

        playerController.SwitchToUI();

        currentUI = title;
        currentUI.SwitchTo();
    }

    // Control inputs

    public void LeftDown()
    {
        currentUI.LeftDown();
    }
    public void LeftUp()
    {
        currentUI.LeftUp();
    }

    public void RightDown()
    {
        currentUI.RightDown();
    }
    public void RightUp()
    {
        currentUI.RightUp();
    }

    public void SpaceDown()
    {
        currentUI.SpaceDown();
    }
    public void SpaceUp()
    {
        currentUI.SpaceUp();
    }

    // Utilities

    private void SubscribeToEvents()
    {
        title.OnChallengeModeSelected += EnterSettings;
        title.OnZenModeSelected += EnterZenMode;

        settings.OnSettingsSet += EnterChallengeMode;

        gameplay.OnGameEnded += EnterEndScreen;

        end.OnExitEndScreen += ReturnToTitle;
    }
}
