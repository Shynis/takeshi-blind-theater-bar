using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class CameraManager : MonoBehaviour
{
    static private CameraManager m_instance;
    static public CameraManager instance { get { return m_instance; } private set { m_instance = value; } }

    [SerializeField] CinemachineVirtualCamera vCam;

    CinemachineBasicMultiChannelPerlin vCamNoise;

    [SerializeField] float amplitudeGain=2;
    [SerializeField] float shakeDuration=0.7f;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        vCamNoise = vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CameraShake()
    {
        vCamNoise.m_AmplitudeGain = amplitudeGain;
        DOTween.To(() => vCamNoise.m_AmplitudeGain,
            x => vCamNoise.m_AmplitudeGain = x, 0, shakeDuration);
    }
}
