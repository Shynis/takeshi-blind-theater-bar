using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PooledPrefab
{
    public GameObject prefab;
    public Vector3 initialPositon;

    public PooledPrefab(GameObject go, Vector3 p)
    {
        prefab = go;
        initialPositon = p;
    }
}

public class PoolManager : MonoBehaviour
{

    static private PoolManager m_instance;
    static public PoolManager instance { get { return m_instance; } private set { m_instance = value; } }

    [SerializeField] List<GameObject> m_propsPool;
    List<PooledPrefab> m_propsPoolprefab;
    [SerializeField] List<GameObject> m_props;
    List<PooledPrefab> m_propsprefab;
    



    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        m_propsprefab = new List<PooledPrefab>();
        m_propsPoolprefab = new List<PooledPrefab>();
        foreach (GameObject prop in m_props)
        {
            m_propsprefab.Add(new PooledPrefab(prop, prop.transform.localPosition));
        }
        foreach (GameObject prop in m_propsPool)
        {
            m_propsPoolprefab.Add(new PooledPrefab(prop, prop.transform.localPosition));
        }
    }

    private void OnDestroy()
    {
        foreach (GameObject prop in m_props)
        {
            Destroy(prop);
        }
        foreach (GameObject prop in m_propsPool)
        {
            Destroy(prop);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public PooledPrefab GetProp()
    {
        int propId = Random.Range(0, m_props.Count);
        return m_propsprefab[propId];
    }

    public PooledPrefab GetPropPool()
    {
        int propId = Random.Range(0, m_propsPool.Count);
        return m_propsPoolprefab[propId];
    }
}
