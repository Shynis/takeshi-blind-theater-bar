using UnityEngine;

public class ControlsManager : MonoBehaviour
{
    private VibeUduino vibeManager;
    private GameManager gameManager;

    private UIManager UImanager;

    private void Start()
    {
        vibeManager = FindObjectOfType<VibeUduino>();
        gameManager = FindObjectOfType<GameManager>();

        UImanager = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            // vibeManager.ActivateLeft();
            UImanager.LeftDown();
        }

        if(Input.GetKeyUp(KeyCode.LeftArrow))
        {
            // vibeManager.DeactivateLeft();
            UImanager.LeftUp();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            // vibeManager.ActivateRight();
            UImanager.RightDown();
        }

        if(Input.GetKeyUp(KeyCode.RightArrow))
        {
            // vibeManager.DeactivateRight();
            UImanager.RightUp();
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            vibeManager.SwitchVibes();
        }

        if(Input.GetKeyDown("space"))
        {
            // gameManager.StartGame();
            UImanager.SpaceDown();
        }
    }
}
