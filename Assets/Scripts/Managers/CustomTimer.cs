using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomTimer : MonoBehaviour
{
    static private CustomTimer m_instance;
    static public CustomTimer instance { get { return m_instance; } private set { m_instance = value; } }

    public bool bIsActive = true;
    public float time { get; private set; }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        GameManager.instance.OnGameStarted += Reset;
    }

    private void Reset()
    {
        time = 0;
    }

    private void Update()
    {
        if (bIsActive)
        {
            time += Time.deltaTime;
        }
    }

    public void ToggleIsActive()
    {
        bIsActive = !bIsActive;
    }
}
