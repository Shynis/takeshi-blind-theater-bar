using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogManager : MonoBehaviour
{
    static private FogManager m_instance;
    static public FogManager instance { get { return m_instance; } private set { m_instance = instance; } }

    [SerializeField] Material m_material;
    [SerializeField] Material m_material2;
    [SerializeField] float fogValue = 0.025f;
    [SerializeField] float contrastFogValue = 0.1f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        GameManager.instance.OnContrastChanged += OnContrastChanged;
        m_material.SetFloat("_UseFog", 1 - GameManager.instance.gameSettings.doorConstrast);
        m_material2.SetFloat("_UseFog", 1 - GameManager.instance.gameSettings.doorConstrast);
    }

    void OnContrastChanged()
    {
        RenderSettings.fogDensity = Mathf.Lerp(fogValue, contrastFogValue, GameManager.instance.gameSettings.doorConstrast);
        m_material.SetFloat("_UseFog", 1 - GameManager.instance.gameSettings.doorConstrast);
        m_material2.SetFloat("_UseFog", 1 - GameManager.instance.gameSettings.doorConstrast);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
