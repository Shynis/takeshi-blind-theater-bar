using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public struct AudioElement
{
    public string name;
    public AudioClip clip;
    public float volume;
}


public class SoundManager : MonoBehaviour
{
    static private SoundManager m_instance;
    static public SoundManager instance { get { return m_instance; } private set { m_instance = instance; } }


    private VibeUduino vibeManager;

    [SerializeField] private AudioSource m_audioSource1;
    [SerializeField] private AudioSource m_sfxAudioSource;
    [SerializeField] private AudioSource m_inputLeftAudioSource;
    [SerializeField] private AudioSource m_inputRightAudioSource;

    [SerializeField] private List<AudioElement> m_sfxList;
    public Dictionary<string, AudioElement> sfxDictionnary;

    [SerializeField] float m_fadeInDuration=0.2f;
    [SerializeField] Vector2 sfxPitchRange = new Vector2(0.8f, 1.5f);



    // Start is called before the first frame update
    void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
        }
        else
        {
            Destroy(this);
        }

        sfxDictionnary = new Dictionary<string, AudioElement>();
        foreach (AudioElement sfx in m_sfxList)
        {
            sfxDictionnary.Add(sfx.name, sfx);
        }
    }


    private void Start()
    {
        vibeManager = FindObjectOfType<VibeUduino>();
        vibeManager.OnActivateLeft += OnActivateLeft;
        vibeManager.OnActivateRight += OnActivateRight;
        vibeManager.OnDeactivateLeft += OnDeactivateLeft;
        vibeManager.OnDeactivateRight += OnDeactivateRight;
        vibeManager.OnSwitchVibe += OnSwitchVibe;

        GameManager.instance.OnDoorPassed += OnPassDoor;
        GameManager.instance.OnFailDoor += OnFailDoor;
        GameManager.instance.OnGameEnded += OnGameEnded;
        GameManager.instance.OnGameStarted += OnGameStarted;
        GameManager.instance.m_panelSpawner.OnPanelSpawn += OnPanelSpawn;
    }

    public void PlaySFX(string sfxName, bool pitchRandomized = false)
    {
        AudioElement audioElement = sfxDictionnary[sfxName];
        if (pitchRandomized)
        {
            m_sfxAudioSource.pitch = Random.Range(sfxPitchRange.x, sfxPitchRange.y);
        }
        else
        {
            m_sfxAudioSource.pitch = 1;
        }

        m_sfxAudioSource.PlayOneShot(audioElement.clip, audioElement.volume);
    }

    public void OnActivateLeft()
    {
        DOTween.To(() => m_inputLeftAudioSource.volume,
            x => m_inputLeftAudioSource.volume = x, 0, m_fadeInDuration);
    }

    public void OnActivateRight()
    {
        DOTween.To(() => m_inputRightAudioSource.volume,
            x => m_inputRightAudioSource.volume = x, 0, m_fadeInDuration);
    }

    public void OnDeactivateLeft()
    {
        DOTween.To(() => m_inputLeftAudioSource.volume,
            x => m_inputLeftAudioSource.volume = x, 0, m_fadeInDuration);
    }

    public void OnDeactivateRight()
    {
        DOTween.To(() => m_inputRightAudioSource.volume,
            x => m_inputRightAudioSource.volume = x, 0, m_fadeInDuration);
    }

    void OnSwitchVibe()
    {
        DOTween.To(() => m_inputRightAudioSource.volume,
            x => m_inputRightAudioSource.volume = x, m_inputLeftAudioSource.volume, m_fadeInDuration);
        DOTween.To(() => m_inputLeftAudioSource.volume,
            x => m_inputLeftAudioSource.volume = x, m_inputRightAudioSource.volume, m_fadeInDuration);
    }

    void OnPassDoor()
    {
        PlaySFX("success", true);
    }

    void OnFailDoor()
    {
        PlaySFX("failure", true);
    }

    void OnGameEnded()
    {
        StartCoroutine(PlaySFXAfterDelay(1, "end", true));
    }

    void OnGameStarted()
    {
        //PlaySFX("start", true);
    }

    void OnPanelSpawn(float duration, float doorPosition, Panel panel)
    {
        StartCoroutine(PlaySFXAfterDelayCustom(Mathf.Max(duration - 3.6f, 0), "start", true));
    }

    IEnumerator PlaySFXAfterDelay(float delay, string name, bool pitchRandomized)
    {
        yield return new WaitForSeconds(delay);
        PlaySFX(name, pitchRandomized);
    }


    IEnumerator PlaySFXAfterDelayCustom(float delay, string name, bool pitchRandomized)
    {
        float timer = delay;
        while (timer>0)
        {
            if (CustomTimer.instance.bIsActive)
            {
                timer -= Time.deltaTime;
            }
            yield return null;
        }
        PlaySFX(name, pitchRandomized);
    }
}
