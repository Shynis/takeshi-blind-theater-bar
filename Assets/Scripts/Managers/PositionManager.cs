using OpenCvSharp;
using OpenCvSharp.Demo;
using UnityEngine;
using Rect = OpenCvSharp.Rect;

public class PositionManager : MonoBehaviour
{
    [SerializeField] private bool InvertImageHorizontal = false;

    [SerializeField] private float time_delay = 0.5f;
    private float time_left = 0f;

    public Vector2 headPosition {get; private set;}
    public bool headFound {get; private set;}

    public Vector2 headPositionScreenSpace { get; private set; }

    private FaceDetectorScene faceDetectorScene;

    public float silhouetteDepth;

    [SerializeField] float scale = 1;

    // Start is called before the first frame update
    private void Awake()
    {
        faceDetectorScene = FindObjectOfType<FaceDetectorScene>();
    }

    private void Start()
    {
        headPosition = new Vector2(0, 0);
        headPositionScreenSpace = new Vector2(0.5f, 0.5f);
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        GetFacesInCamera();
    }

    private void GetFacesInCamera()
    {
        Vector2 textureSize = faceDetectorScene.TextureSize;
        Vector2 factor = new Vector2(Screen.width / textureSize.x, Screen.height / textureSize.y);

        if(faceDetectorScene.Faces.Count > 0 )
        {
            Rect rect = faceDetectorScene.Faces[0];

            // Debug.Log("---------------------------");
            // Debug.Log("rect raw: " + rect.Center.X + ", " + rect.Center.Y);

            rect.Size = new Size(rect.Size.Width * factor.x, rect.Size.Height * factor.y);
            rect.X = (int) (rect.X * factor.x);
            rect.Y = Screen.height - (int) (rect.Y * factor.y);

            if(InvertImageHorizontal)
            {
                rect.X = Screen.width - rect.X;
            }

            Vector2 rectCenter = new Vector2(Mathf.Clamp((int)((rect.Center.X - Screen.width / 2) * scale + Screen.width / 2), 0, Screen.width -1),
                Mathf.Clamp((int)((rect.Center.Y - Screen.height / 2) * scale + Screen.height / 2), 0, Screen.height - 1));

            headPositionScreenSpace = new Vector2(rectCenter.x/(float)Camera.main.pixelWidth, rectCenter.y/(float)Camera.main.pixelHeight);

            headPosition = Camera.main.ScreenToWorldPoint(new Vector3(rectCenter.x, rectCenter.y, silhouetteDepth));

            // Debug.Log("rect changed: " + rect.Center.X + ", " + rect.Center.Y);

            headFound = true;

            time_left = time_delay;
        }
        else
        {
            headFound = false;

            time_left -= Time.deltaTime;
            if(time_left < 0)
            {
                // Pause game with a message saying that they left the zone
            }
        }
    }
}
