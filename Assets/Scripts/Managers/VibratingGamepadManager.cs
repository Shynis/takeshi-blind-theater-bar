using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class VibratingGamepadManager : MonoBehaviour
{
    public float lowFrequency = 0.5f;
    public float highFrequency = 0.5f;

    private VibratingGamepad gamepadLeft;
    private VibratingGamepad gamepadRight;

    // Start is called before the first frame update
    void Start()
    {
        ReadOnlyArray<Gamepad> gamepadList = Gamepad.all;

        if(gamepadList.Count >= 2)
        {
            gamepadLeft = new VibratingGamepad(gamepadList[0]);
            gamepadRight = new VibratingGamepad(gamepadList[1]);
        }
        else
        {
            Debug.LogWarning("Missing gamepad");
        }

        InputSystem.ResumeHaptics();
    }

    private void OnDestroy()
    {
        InputSystem.ResetHaptics();
    }

    public void SwitchControllers()
    {
        VibratingGamepad tempGamepad = gamepadLeft;

        gamepadLeft = gamepadRight;
        gamepadRight = tempGamepad;

        InputSystem.ResetHaptics();
    }

    public void StopVibrations()
    {
        InputSystem.ResetHaptics();
    }

    public void SwitchOnLeft()
    {
        gamepadLeft.SwitchOn();
    }

    public void SwitchOnRight()
    {
        gamepadRight.SwitchOn();
    }

    public void ActivateLeft()
    {
        Debug.Log("Activate left");
        gamepadLeft.Activate();
    }

    public void DeactivateLeft()
    {
        Debug.Log("Deactivate left");
        gamepadLeft.Deactivate();
    }

    public void ActivateRight()
    {
        Debug.Log("Activate right");
        gamepadRight.Activate();
    }

    public void DeactivateRight()
    {
        Debug.Log("Deactivate right");
        gamepadRight.Deactivate();
    }

    public void UpdateVibrationFreq()
    {
        gamepadLeft.SetFrequency(lowFrequency, highFrequency);
        gamepadRight.SetFrequency(lowFrequency, highFrequency);
    }
}
