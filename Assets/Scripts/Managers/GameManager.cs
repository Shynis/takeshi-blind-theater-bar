using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        StartMenu,
        SettingsMenu,
        GamePlaying,
        EndMenu,
        COUNT
    }

    public enum DoorManual
    {
        Validate,
        Discard,
        None
    }

    [System.Serializable] public class GameSettings
        {
        public int gameSpeed = 1;
        public float tolerance = 0.2f;
        public float doorConstrast = 0f;
        public bool zenModeSettings = false;
    }

    static private GameManager m_instance;
    static public GameManager instance { get { return m_instance; } private set { m_instance = value; } }

    public PanelSpawner m_panelSpawner;
    [SerializeField] private PositionManager m_positionManager;

    [Header("Game state")]
    [SerializeField] private int lifeNumber = 3;
    public int HeartLeft { get; private set; }
    public int Score { get; private set; }

    [Header("Game Settings")]
    [SerializeField] private GameSettings m_gameSettings;
    public GameSettings gameSettings { get { return m_gameSettings; } private set { m_gameSettings = value; } }

    public GameState CurrentState {get; private set; }

    [Header("Difficulty Evolution")]
    [SerializeField] private List<float> m_difficultySettingsMultiplier;
    public List<float> difficultySettingsMultiplier { get { return m_difficultySettingsMultiplier; } private set {m_difficultySettingsMultiplier = value; } }

    [SerializeField] private AnimationCurve m_speedMultiplierCurve;
    public AnimationCurve speedMultiplierCurve { get { return m_speedMultiplierCurve; } private set { m_speedMultiplierCurve = value; } }

    [SerializeField] private Vector2 m_speedMultiplierCurveScale;
    public Vector2 speedMultiplierCurveScale { get { return m_speedMultiplierCurveScale; } private set { m_speedMultiplierCurveScale = value; } }


    [Header("ToleranceMapping")]
    [SerializeField] Vector2 toleranceMapping = new Vector2(0.05f, 0.5f);

    public delegate void EventAction();
    public event EventAction OnPassDoor;
    public event EventAction OnFailDoor;
    public event EventAction OnContrastChanged;

    public DoorManual doorManualInput;

    

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_panelSpawner.OnPanelSpawn += OnSpawn;

        // Move to somewhere better ?
        CustomTimer.instance.bIsActive = false;

        doorManualInput = DoorManual.None;
    }

    void OnSpawn(float duration, float doorPosition, Panel panel)
    {
        StartCoroutine(PanelSuccessCheckCoroutine(duration, doorPosition, panel));
    }

    IEnumerator PanelSuccessCheckCoroutine(float duration, float doorPosition, Panel panel)
    {
        while ((CustomTimer.instance.time - panel.spawnDate) < panel.duration)
        {
            if (IsZenModeActivated() && doorManualInput == DoorManual.None)
            {
                CustomTimer.instance.bIsActive = IsInRange(doorPosition);
            }
            else
            {
                CustomTimer.instance.bIsActive = true;
            }
            yield return null;
        }
        //Debug.Log("player: " + m_positionManager.headPositionScreenSpace);
        //Debug.Log("door: " + doorPosition);
        switch (doorManualInput)
        {
            case DoorManual.Validate:
                DoorPassed();
                break;


            case DoorManual.Discard:
                DoorFailed();
                break;

            default:
                if (Mathf.Abs(m_positionManager.headPositionScreenSpace.x - doorPosition) < GetTolerance())
                {
                    DoorPassed();
                }
                else
                {
                    DoorFailed();
                }
                break;
        }
        doorManualInput = DoorManual.None;
        
    }

    public void StartGame()
    {
        InitializeGame();

        CurrentState = GameState.GamePlaying;
    }

    public void SetGameSpeed(int val)
    {
        gameSettings.gameSpeed = val;
    }

    public void SetPrecision(float val)
    {
        gameSettings.tolerance = val;
    }

    public void SetContrast(float val)
    {
        gameSettings.doorConstrast = val;
        OnContrastChanged?.Invoke();
    }

    // Initialize game session

    public event Action OnGameStarted;
    private void InitializeGame()
    {
        HeartLeft = lifeNumber;
        Score = 0;
        CustomTimer.instance.bIsActive = true;

        OnGameStarted?.Invoke();
    }

    public event Action OnHeartLost;
    public event Action OnDoorPassed;
    public event Action OnGameEnded;

    private void PassDoor()
    {
        Score += 1;
        Debug.Log("score: " + Score);

        OnDoorPassed?.Invoke();
    }

    private void LoseHeart()
    {
        HeartLeft--;

        OnHeartLost?.Invoke();

        if(HeartLeft <= 0)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        Debug.Log("End game");

        CustomTimer.instance.bIsActive = false;

        OnGameEnded?.Invoke();

        CurrentState = GameState.EndMenu;
    }

    private bool IsInRange(float doorPosition)
    {
        return Mathf.Abs(m_positionManager.headPositionScreenSpace.x - doorPosition) < m_gameSettings.tolerance;
    }

    private bool IsZenModeActivated()
    {
        if (m_panelSpawner.panelSpawned <= 1)
        {
            return true;
        }
        else
        {
            return gameSettings.zenModeSettings;
        }
    }

    float GetTolerance()
    {
        return Mathf.Lerp(toleranceMapping.x, toleranceMapping.y, gameSettings.tolerance);
    }


    public void OnManuallyValidateDoor()
    {
        doorManualInput = DoorManual.Validate;
    }

    public void OnManuallyCancelDoor()
    {
        doorManualInput = DoorManual.Discard;
    }

    void DoorPassed()
    {
        Debug.Log("Success");

        PassDoor();
        OnPassDoor?.Invoke();
    }

    void DoorFailed()
    {
        Debug.Log("Fail");

        LoseHeart();
        OnFailDoor?.Invoke();
    }
}
